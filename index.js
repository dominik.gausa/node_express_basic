const express = require('express'); 
const app = express(); 
const path = require('path');

app.get('/', function (req, res) {
    res.redirect('/index.htm');
});

app.get('/hi', function (req, res) {
    res.send('Hello World!');
});

app.use('/', express.static(path.join(__dirname, 'static')));

app.use('/api', require('./api.js'));
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

